// Variables:
// Data storage
//
//
//To declare a variable, we'll us ethe keywords let or const
//
let student = "Brandon";
console.log(student);

const day ="Monday";
//console.log(day);

//Const is short for constant
//we cannot reassign a value

student = 3.1416;
// console.log(student);

console.log(typeof "am I a string?");

const num1 = 10;
const num2 = 2;
const num3 = 6;
console.log(num1 + num2)

let output = 0;
const num4 = 5;
const num5 = 8;

